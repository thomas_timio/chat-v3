export default class Message{
    texte = "";
    pseudo = "";
    date = "";
    partie = "";

    constructor(texte,pseudo,date,partie) {
        this.texte = texte
        this.pseudo = pseudo
        this.date = date
        this.partie = partie
    }
}