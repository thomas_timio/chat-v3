import React from 'react';
import MessageRender from './MessageRender';

const MessageList = (props) => {
    return (
        props.historique.map((message, index) => MessageRender(message, index))
    );
};

export default MessageList;