import React from 'react';

const Formulaire = (props) => {
    const handleKeyUp = event => {
        if (event.key === 'Enter') {
            return props.add
        }
    }
    return (
        <form className = 'entreeMessage' onSubmit = {props.submit} >
            <input type = 'text' onChange={props.change} value={props.value.texte} onKeyUp={handleKeyUp}></input>
            <button>Clique</button>
        </form>
    );
};

export default Formulaire;