import React from 'react';

const MessageRender = (props) => {
    return (
        <p key={props.index}>{props.pseudo} : {props.texte}</p>
    );
};

export default MessageRender;