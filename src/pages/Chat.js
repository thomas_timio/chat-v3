import React, { Fragment } from 'react';
import MessageList from '../components/MessageList'
import { useState } from 'react'
import Formulaire from '../components/Formulaire'
import Message from '../models/Message'

const App = () => {
    const [historique, setHistorique] = useState([])
    const [message, setMessage] = useState(new Message())

    
    
    /**
     * on enregistre le texte du message de l'input de formulaire dans le state message
     * @param {*} event correspond à chaque lettre tapée par un utilisateur
     */
    const handleChange = (event) => {
        setMessage({texte : event.target.value})
    }

    /**
     * on appelle la fonction addMessage
     * @param {*} event quand l'utilisateur clique sur le bouton du formulaire
     */
    const handleSubmit = (event) => {
        event.preventDefault()
        addMessage()
    }

    /**
     * On ajoute le message à la liste des messages enregistrés
     */
    const addMessage = () => {
        setHistorique([...historique, {texte : message.texte, pseudo : "Thomas", date : Date.now(), partie : "test"}])
        setMessage({texte:""})
    }
    return (
        <Fragment>
            <MessageList historique = {historique} />
            <Formulaire value={message} change={handleChange} submit={handleSubmit} add={addMessage}/>
        </Fragment>
    );
};

export default App;